@extends('layouts.navbar')
@section('navi')

    <h1 class="mb-4 text-center">Registration</h1>
    <form action="/register" method="post">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control @error('name')is-invalid @enderror" name="name" id="name">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control @error('email')is-invalid @enderror" name="email" id="email">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control @error('password')is-invalid @enderror" name="password" id="password">
        </div>
        <input type="submit" class="btn btn-primary mb-3" name="login" value="Register">
    </form>
    <small>Already registered? <a href="/login">Login</a></small>
@endsection
